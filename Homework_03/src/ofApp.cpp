#include "ofApp.h"

using namespace ofxCv;
using namespace cv;

//--------------------------------------------------------------
void adjustGamma(cv::Mat& img, float gamma = 0.5) {
    cv::Mat lookUpTable(1, 256, CV_8U);
    unsigned char* p = lookUpTable.ptr();
    for (int i = 0; i < 256; i++) {
        p[i] = saturate_cast<unsigned char>(pow(i / 255.0, gamma) * 255.0);
    }
    cv::LUT(img, lookUpTable, img);
}

void ofApp::setup(){

//    cam.initGrabber(640, 480);

    cam.setup(640, 480);
    frame.allocate(800,600,OF_IMAGE_COLOR);
    gui.setup();
    gui.add(squaresize.setup("suqare size", 1, 1, 10));
    gui.add(colormode.setup("color mode", 3, 1, 3));
    gui.add(filled.setup("fill",true));


}

//--------------------------------------------------------------
void ofApp::update(){
    if(cam.isInitialized()){
    cam.update();
    }
    if(cam.isFrameNew()){
        frame.setFromPixels(cam.getPixels());
      img = toCv(cam);
        float gamma = ofMap(mouseX, 0, ofGetWidth(), 0, 2);
        adjustGamma(img, gamma);
        
    }
    

}

//--------------------------------------------------------------
void ofApp::draw(){
    
    cam.draw(0, 0);
  
//    frame.draw(0,0);
   ofxCv::drawMat(img, 0, 0);
    
    for(int x = 5;x<635;x+=10){
        for(int y = 5;y<475;y+=10){
            ofColor mycol = frame.getColor(x, y);
            ofSetColor(frame.getColor(x, y));
            mycol.setSaturation(220);
            
//            float bright = frame.getColor(x,y).getBrightness();
//            ofSetColor(bright);
            if(y%colormode==0){
                
                
                mycol.setHue(mycol.getHue()-y);
                ofSetColor(mycol);
                
            }
            if(filled){
                ofFill();
            }else{
                ofNoFill();
            }
            
            ofDrawRectangle(x, y, 1*squaresize, 1*squaresize);
            
//            ofDrawEllipse(x, y, 10*squaresize,10*squaresize);
            
            
        }
    }
    gui.draw();
    

}


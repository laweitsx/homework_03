#pragma once
#include "ofMain.h"
#include "ofxGui.h"
#include "ofxCv.h"

class ofApp : public ofBaseApp{

    public:
        void setup();
        void update();
        void draw();

        
    ofVideoGrabber cam;

    ofImage frame;
    ofxPanel gui;
    ofxIntSlider colormode;
    ofxIntSlider squaresize;
    ofxToggle filled;
    cv::Mat img;
    
    
        
};
